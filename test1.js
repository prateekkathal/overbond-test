const benchmarkCalculator = require("./util/benchmarkCalculator");

// Mock Data
const corporateBonds = [
  {
    bond: 'C1',
    type: 'corporate',
    term: 10.3,
    yield: 5.30
  },
];
const governmentBonds = [
  {
    bond: 'G1',
    type: 'government',
    term: 9.4,
    yield: 3.70
  },
  {
    bond: 'G2',
    type: 'government',
    term: 12,
    yield: 4.80
  },
];

// Test Case 1
const benchmarkBonds = benchmarkCalculator.multiple(corporateBonds, governmentBonds);
if (benchmarkBonds[0].spread_to_benchmark == 1.60) {
  console.log("Spread to Benchmark Value - Success");
} else {
  console.log("Spread to Benchmark Value - Failed");
}
