const csvToJson = require("csvtojson");
const parseBondValues = require("./util/parseBondValues");
const benchmarkCalculator = require("./util/benchmarkCalculator");

// CSV File
const pathToCsv = 'data/final.csv';

// Coverts to JSON
csvToJson()
  .fromFile(pathToCsv)
  .then((bonds) => {
    // Parses bond term & yield values from string to float.
    bonds = parseBondValues(bonds);

    // Filter bonds by type
    const corporateBonds = bonds.filter(bond => bond.type === 'corporate');
    const governmentBonds = bonds.filter(bond => bond.type === 'government');

    // Find benchmark for bonds
    const benchmarkBonds = benchmarkCalculator.multiple(corporateBonds, governmentBonds);

    console.log(benchmarkBonds);
  })
