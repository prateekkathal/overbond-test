# Overbond

## Technologies

1. NodeJS (12+)

## Packages

1. [CSVtoJSON](https://www.npmjs.com/package/csvtojson) - `csvtojson` (v2.0.10)
2. [LERP Function](https://www.npmjs.com/package/lerp) - `lerp` (v1.0.3)

## Installation

1. Please ensure you have NodeJS 12+ installed
2. Finally clone the repo

```
git clone ...
```

3. Install packages using yarn or npm

```
yarn
// or npm i
```

4. Run the file `challenge1.js` for _Challenge 1_ & `challenge2.js` for _Challenge 2_.

```
node challenge1.js
node challenge2.js
```

Note: I used the sample tables provided for coding at first, but finally added the `final.csv` & replaced it in both files.

## Reasoning & Trade-Offs

#### Reasoning

1. Why NodeJS?
   - Easier to setup
   - Installation of packages is easy
   - More experience/familiarity in Javascript than Ruby.
   - Could've also used PHP, but it is harded to set up locally (at least for the person testing this).
2. Why so many files & lines?
   - More readability
   - Code structuring
3. Approach To Benchmark
   - Loop through all government bonds.
   - Find the one with the smallest difference in comparision to a corporate.
   - Use matched bond to calculate _Spread to Benchmark_
4. Approach To Linear Interpolation
   - Loop through all government bonds.
   - Find government bond with a slightly smaller bond term value.
   - Find government bond with a slightly greater bond term value.
   - Find Point of Interpolation.
   - Use the Linear Interpolation formula & use it to calculate _Spread to Curve_

#### Trade-Offs

1. Challenge 1
   - The first challenge was easy. Didn't find much issues coding it.
   - I would however would spend 30-40 mins more to decrease the number of lines.
2. Challenge 2
   - This was not easy since I am unfamiliar with linear interpolation.
   - I however googled about it a lot & spent an hour and a half just trying to figure how exactly to find the point of interpolation.
   - At the end, I was able to figure out the way to find the point, but had to use the `lerp` package to handle the calculation of interpolation.
   - Again, I would spend more time structuring the code better & probably doing smallest & lowest in just one function (for performance).
3. Lastly, I prefer using Typescript so I would've added that to make the code more robust & developer friendly. However writing typescript consumes more time than regular JS.
4. I didn't use all ES6 syntaxes. Wanted to start asap & didn't want to face issues with libraries that didn't support ES6.
5. I would've added a docker instance in case NodeJS wasn't available on testing environment. But keeping code structuring in mind, I didn't really initiate the though process for it until the end of the exam.
6. Could've used a testing library but didn't have much time (had to write all the documentation), so, I simply asserted reponses using mock data.

## Sources

1. Wiki - https://en.wikipedia.org/wiki/Linear_interpolation
2. StackOverflow - https://stackoverflow.com/questions/42392535/linear-interpolation-library-with-php
3. Data Digitization - https://www.datadigitization.com/dagra-in-action/linear-interpolation-with-excel/

## Overall Review

The test I'd say was easy overall. It required research from my end to figure out the formulae to equations but I found it super fun & intresting.

I personally have always been working on building CRUDs or working on the front-end. It is not mandatory for each developer to know or remember formulas, especially the ones which we don't use everyday. As was the case for me as well.

I liked it all in all.

## Contributors

1. Prateek Kathal - [Github](https://github.com/prateekkathal) | [Bitbucket](https://bitbucket.org/prateekkathal/)
