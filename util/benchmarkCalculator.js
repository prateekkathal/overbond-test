module.exports = {
  single: calculateBenchmark,
  multiple: calculateBenchmarks
}

/**
 * Calculate multiple corporate bond benchmarks
 * 
 * @param {*} corporateBonds All corporate bonds
 * @param {*} governmentBonds All government bonds
 */
function calculateBenchmarks(corporateBonds, governmentBonds) {
  // Runs the function below & filters out bonds whose benchmarks
  // were actually found.
  return corporateBonds.map(corporateBond => {
    return calculateBenchmark(corporateBond, governmentBonds);
  }).filter(bond => bond);
}

/**
 * Calculate corporate bond's benchmark
 * 
 * @param {*} corporateBonds One corporate bond
 * @param {*} governmentBonds All government bonds
 */
function calculateBenchmark(corporateBond, governmentBonds) {
  let benchmarkBondDifference = 0;
  let bencharmarkBondIndex = null;

  // Maps each value & find matching benchmark bond.
  governmentBonds.map((bond, bondIndex) => {
    const bondDifference = parseFloat((corporateBond.term - bond.term).toFixed(2));

    // Not sure if this was supposed to be absolute value of bondDifference or greater than 0.
    // Taking "bondDifference > 0" just in case.
    if (!bondIndex || (bondDifference > 0 && bondDifference < benchmarkBondDifference)) {
      benchmarkBondDifference = bondDifference;
      bencharmarkBondIndex = bondIndex;
    }
  });

  // In case no bonds were found, for example (no government bonds in list)
  if (bencharmarkBondIndex === null) {
    return null;
  }

  const benchmarkBond = governmentBonds[bencharmarkBondIndex];

  // Final benchmark government bond
  return {
    bond: corporateBond.bond,
    benchmark: benchmarkBond.bond,
    spread_to_benchmark: (corporateBond.yield - benchmarkBond.yield).toFixed(2),
  };
}
