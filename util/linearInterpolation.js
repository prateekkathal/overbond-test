module.exports = {
  calculateSmaller,
  calculateGreater,
  calculateInterpolationPoint,
}

/**
 * Get a bond with a term smaller than corporate bond term
 * to calculate point of interpolation.
 * 
 * @param {*} corporateBond Single Bond
 * @param {*} governmentBonds All government bonds
 */
function calculateSmaller(corporateBond, governmentBonds) {
  const allBondTerms = governmentBonds.map(bond => bond.term);
  smallerGbTerm = Math.min(...allBondTerms);
  bondIndex = governmentBonds.findIndex(bond => bond.term === smallerGbTerm);

  // Map all government bonds to find the second smaller to the corporate bond term.
  governmentBonds.map((bond, index) => {
    const cbTerm = corporateBond.term;
    const gbTerm = bond.term;

    if (gbTerm < cbTerm && smallerGbTerm < gbTerm) {
      smallerGbTerm = gbTerm;
      bondIndex = index;
    }
  });

  // No error handling in this case since question mentioned it'll always be there.
  // Didn't have much time to debug

  return governmentBonds[bondIndex];
}

/**
 * Get a bond with a term smaller than corporate bond term
 * to calculate point of interpolation.
 * 
 * @param {*} corporateBond Single Bond
 * @param {*} governmentBonds All government bonds
 */
function calculateGreater(corporateBond, governmentBonds) {
  const allBondTerms = governmentBonds.map(bond => bond.term);
  greaterGbTerm = Math.max(...allBondTerms);
  bondIndex = governmentBonds.findIndex(bond => bond.term === greaterGbTerm);

  // Map all government bonds to find the second greater to the corporate bond term.
  governmentBonds.map((bond, index) => {
    const cbTerm = corporateBond.term;
    const gbTerm = bond.term;

    if (cbTerm < gbTerm && gbTerm < greaterGbTerm) {
      greaterGbTerm = gbTerm;
      bondIndex = index;
    }
  });

  // No error handling in this case since question mentioned it'll always be there.
  // Didn't have much time to debug

  return governmentBonds[bondIndex];
}

/**
 * Calculate the point of interpolation.
 * 
 * @param {*} corporateBond Single Bond
 * @param {*} smallerBond Single Bond
 * @param {*} greaterBond Single Bond
 */
function calculateInterpolationPoint(corporateBond, smallerBond, greaterBond) {
  return (corporateBond.term - smallerBond.term) / (greaterBond.term - smallerBond.term);
}
