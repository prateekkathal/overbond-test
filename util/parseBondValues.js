module.exports = (parsedJson) => {
  return parsedJson.map(bond => ({
    ...bond,
    term: parseFloat(bond.term.replace(' years', '')),
    yield: parseFloat(bond.yield.replace('%', '')),
  }))
}
