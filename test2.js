const lerp = require("lerp");
const lI = require("./util/linearInterpolation");

// Mock Data
const corporateBonds = [
  {
    bond: 'C1',
    type: 'corporate',
    term: 10.3,
    yield: 5.30
  },
  {
    bond: 'C2',
    type: 'corporate',
    term: 15.2,
    yield: 8.30
  },
];
const governmentBonds = [
  {
    bond: 'G1',
    type: 'government',
    term: 9.4,
    yield: 3.70
  },
  {
    bond: 'G2',
    type: 'government',
    term: 12,
    yield: 4.80
  },
  {
    bond: 'G2',
    type: 'government',
    term: 16.3,
    yield: 5.50,
  },
];

// Test Case 1
const poi = lI.calculateInterpolationPoint(corporateBonds[0], governmentBonds[0], governmentBonds[1]);
if (parseFloat(poi.toFixed(3)) === 0.346) {
  console.log("Point of Interpolation Value - Success");
} else {
  console.log("Point of Interpolation Value - Failed");
}

// Test Case 2
let lIValue = lerp(governmentBonds[0].yield, governmentBonds[1].yield, poi);
lIValue = parseFloat(lIValue.toFixed(2));

if (lIValue === 4.08) {
  console.log("Linear Interpolation Value - Success");
} else {
  console.log("Linear Interpolation Value - Failed");
}

// Test Case 3
const spreadToCurve = corporateBonds[0].yield - lIValue
if (parseFloat(spreadToCurve.toFixed(2)) === 1.22) {
  console.log("Spread to Curve Value - Success");
} else {
  console.log("Spread to Curve Value - Failed");
}
