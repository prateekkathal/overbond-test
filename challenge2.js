const lerp = require("lerp");
const csvToJson = require("csvtojson");
const lI = require("./util/linearInterpolation");
const parseBondValues = require("./util/parseBondValues");

// CSV File
const pathToCsv = 'data/final.csv';

// Coverts to JSON
csvToJson()
  .fromFile(pathToCsv)
  .then((bonds) => {
    // Parses bond term & yield values from string to float.
    bonds = parseBondValues(bonds);

    // Filter bonds by type
    const corporateBonds = bonds.filter(bond => bond.type === 'corporate');
    const governmentBonds = bonds.filter(bond => bond.type === 'government');

    // Find linear interpolation for bonds
    const corporateBondsWithSTC = corporateBonds.map(corporateBond => {
      // Calculate poi (Point of Interpolation)
      const greaterBond = lI.calculateGreater(corporateBond, governmentBonds);
      const smallerBond = lI.calculateSmaller(corporateBond, governmentBonds);
      const poi = lI.calculateInterpolationPoint(corporateBond, smallerBond, greaterBond);

      // Linear Interpolation Value
      let lIValue = lerp(smallerBond.yield, greaterBond.yield, poi);
      lIValue = parseFloat(lIValue.toFixed(2));

      // Spread To Curve
      const spreadToCurve = corporateBond.yield - lIValue;

      // Final JSON
      return {
        bond: corporateBond.bond,
        spread_to_curve: `${spreadToCurve.toFixed(2)}%`,
      }
    });

    console.log(corporateBondsWithSTC);
  })
